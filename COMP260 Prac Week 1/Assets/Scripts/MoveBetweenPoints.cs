﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenPoints : MonoBehaviour
{
    public Transform pointA, pointB;
    Transform currentTarget;
    public float speed = 1;
    float distanceToMove;

	void Start ()
    {
        currentTarget = pointA;
	}
	

	void FixedUpdate ()
    {

        checkDistance();
        move();
        print(distanceToMove);
	}

    void checkDistance()
    {
        if (Vector3.Distance(transform.position, currentTarget.position) <= 1)
        {
           currentTarget = (currentTarget == pointA) ?  pointB :  pointA;
        }
    }
    void move()
    {
        distanceToMove = Vector3.Distance(transform.position, currentTarget.position);
        transform.position += (currentTarget.position - transform.position).normalized * speed;
    }
}
